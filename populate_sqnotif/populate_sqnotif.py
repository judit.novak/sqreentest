import json
import logging
from flask import abort, request

from . import app, config, targets_dict
from .helpers import check_signature, pretty_data


logger = logging.getLogger(__name__)


@app.route('/populate_sqnotif', methods=['POST'])
def populate_sqnotif():
    """Webhook reading Sqreen notifications redirecting
    them to configured targets"""

    logger.debug("Verifying signature")
    request_body = request.get_data()
    request_signature = request.headers['X-Sqreen-Integrity']
    secret_key = config.get('DEFAULT', 'webhook_key')

    if not (secret_key and request_signature):
        return ("Missing parameters: 'secret_key' "
                "and 'request_signature' are both mandatory.", 422)

    if not check_signature(secret_key, request_signature, request_body):
        abort(401)
    logger.debug("Signature OK")

    # Empty requests to be ignored
    request_data = json.loads(request_body)
    if not request_data:
        return "", 200

    for target in config.get('DEFAULT', 'targets').split(','):
        logger.info("Publishing data to target %s", target)
        success = targets_dict[target].populate(request_data)
        if not success:
            logger.error("Failed to publish data to target %s message\n%s",
                         target, pretty_data(request_data))
            abort(500)
    return '', 200
