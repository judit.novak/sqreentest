import configparser
import hmac
import hashlib
import json

from .exceptions import PopulateException


# Stolen directly from https://docs.sqreen.com/integrations/webhooks/
# Not tested
def check_signature(secret_key, request_signature, request_body):
    hasher = hmac.new(bytes(secret_key, 'latin-1'),
                      request_body,
                      hashlib.sha256)
    dig = hasher.hexdigest()
    return hmac.compare_digest(dig, request_signature)


def check_config(config, keys, section='DEFAULT'):
    for key in keys:
        try:
            if not config.get(section, key):
                raise PopulateException("Missing configuration "
                                        f"value for '{key}'")
        except configparser.NoOptionError:
            raise PopulateException(f"Missing configuration parameter '{key}'")


def pretty_data(data):
    """Pretty string representation of dictionaries"""
    return json.dumps(data, sort_keys=True, indent=4)
