import configparser
import importlib
import logging

from flask import Flask
from .exceptions import PopulateException


def get_config():
    """Reading the config file"""
    config = configparser.ConfigParser()
    config.read('populate_sqnotif/populate_sqnotif.cfg')
    return config


def init_app(config):
    """Verification of mandatory parameters both for
       the application and configured targets, together
       with initialization of each population target"""

    # Avoiding circular import
    from .helpers import check_config
    check_config(config, ['webhook_key', 'targets'])

    targets_dict = {}
    for target in config.get('DEFAULT', 'targets').split(','):

        # Making an attempt to import configured targets
        try:
            module = importlib.import_module(f'.targets.{target}',
                                             'populate_sqnotif')
            targets_dict[target] = module.target_class(config)
        except ImportError:
            raise PopulateException(
                f"Populate target '{target}' doesn't exist"
            )

        # Verifying that all mandatory parameters are configured
        check_config(config,
                     targets_dict[target].fields,
                     targets_dict[target].config_section)

    return targets_dict


# Application-wide variables

app = Flask(__name__)
config = get_config()
targets_dict = init_app(config)

logging.basicConfig(level=config.get('DEFAULT', 'log_level'))
