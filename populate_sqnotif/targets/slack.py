import json
import logging
import requests

from .target import Target
from ..helpers import pretty_data


logger = logging.getLogger(__name__)


class SlackTarget(Target):

    fields = ['webhook_url']
    config_section = 'target_slack'

    def populate(self, message_dict):
        """POST-ing JSON body as a string to Slack webhook"""

        slack_url = self.config.get(self.config_section, 'webhook_url')
        data = json.dumps({"text": pretty_data(message_dict)})

        try:
            resp = requests.post(slack_url, data=data)
        except requests.exceptions.RequestException as err:
            logger.error("Couldn't publish message: %s", str(err))
            return False

        if not resp.status_code == 200:
            logger.error("Couldn't publish message: %s", resp.text)
            return False

        return True


target_class = SlackTarget
