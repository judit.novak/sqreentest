from email.message import EmailMessage
import logging
import smtplib

from .target import Target
from ..helpers import pretty_data


logger = logging.getLogger(__name__)


class EmailTarget(Target):

    config_section = 'target_email'
    fields = ['smtp_server', 'smtp_port', 'to_address', 'from_address']

    def populate(self, message_dict):
        """Sending json_body as email to the configured address"""

        # Connect to SMTP server
        smtp_server = self.config.get(self.config_section, 'smtp_server')
        smtp_port = self.config.get(self.config_section, 'smtp_port')
        try:
            server = smtplib.SMTP(smtp_server, smtp_port)
        except smtplib.SMTPException as err:
            logger.error("Couldn't connect to SMTP server: '%s'", str(err))

        # Create email message
        msg = EmailMessage()
        msg.set_content(pretty_data(message_dict))
        msg['From'] = self.config.get(self.config_section, 'from_address')
        msg['To'] = self.config.get(self.config_section, 'to_address')
        msg['Subject'] = 'Sqreen alert'

        # Send email message
        try:
            server.send_message(msg)
        except smtplib.SMTPException as err:
            logger.error("Couldn't send message: '%s'", str(err))
            return False
        return True


target_class = EmailTarget
