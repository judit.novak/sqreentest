class Target:
    """Generic interface to define Target behaviour"""

    fields = []
    config_section = ''

    def __init__(self, config):
        self.config = config

    def populate(self, message_dict):
        raise NotImplementedError
