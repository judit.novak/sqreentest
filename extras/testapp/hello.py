from flask import Flask, request

import sqreen


sqreen.start()

app = Flask(__name__)

@app.route("/hello")
def hello():
    print(request.json)
    return "Hello, World!", 200
