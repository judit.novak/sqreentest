import configparser
import pytest

from populate_sqnotif import init_app
from populate_sqnotif.exceptions import PopulateException


def test_init_app_default():
    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'non-existing-target'
    }
    with pytest.raises(PopulateException) as err:
        init_app(config)
    assert str(err.value) == ("Populate target 'non-existing-target' "
                              "doesn't exist")


def test_init_app_targets_missing_parameter():
    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    config['target_email'] = {
        'to_address': 'x',
        'smtp_server': 'x',
        'smtp_port': 'x'
    }
    with pytest.raises(PopulateException) as err:
        init_app(config)
        assert "Missing configuration parameter 'from_address'" in err.value


def test_init_app_targets_missing_value():
    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    config['target_email'] = {
        'to_address': '',
    }
    with pytest.raises(PopulateException) as err:
        init_app(config)
        assert "Missing configuration value for 'to_address'" in err.value
