import configparser
import json
import mock
import pytest

from populate_sqnotif import config
from populate_sqnotif import targets_dict
from populate_sqnotif.populate_sqnotif import app


@pytest.fixture
def client():
    return app.test_client()


@mock.patch('populate_sqnotif.config', configparser.ConfigParser())
@mock.patch('populate_sqnotif.helpers.hmac.compare_digest',
            return_value=True)
@mock.patch('populate_sqnotif.targets.email.EmailTarget.populate',
            return_value=True)
def test_allowed_methods(mock_signature, mock_email, client):
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    assert client.get('/populate_sqnotif').status_code == 405
    assert client.put('/populate_sqnotif').status_code == 405

    resp = client.post('/populate_sqnotif',
                       headers={'X-Sqreen-Integrity': 'x'},
                       content_type='application/json',
                       data=json.dumps({'a': 'b'}))
    assert resp.status_code == 200


def test_required_fields(client):
    resp = client.post('/populate_sqnotif',
                       headers={'X-Sqreen-Integrity': ''})
    assert resp.status_code == 422
    assert "Missing parameters" in resp.data.decode('utf-8')


@mock.patch('populate_sqnotif.config', configparser.ConfigParser())
@mock.patch('populate_sqnotif.helpers.hmac.compare_digest',
            return_value=True)
def test_ignore_empty_request(mock_signature, client, mocker):
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    mocker.spy(targets_dict['email'], 'populate')
    resp = client.post('/populate_sqnotif',
                       headers={'X-Sqreen-Integrity': 'x'},
                       content_type='application/json',
                       data=json.dumps({}))
    assert resp.status_code == 200
    assert targets_dict['email'].populate.call_count == 0


@mock.patch('populate_sqnotif.config', configparser.ConfigParser())
@mock.patch('populate_sqnotif.helpers.hmac.compare_digest',
            return_value=True)
@mock.patch('populate_sqnotif.targets.email.EmailTarget.populate',
            return_value=True)
def test_populate_sqnotif(mock_signature, mock_email, client):

    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'non-existing-target'
    }
    resp = client.post('/populate_sqnotif',
                       headers={'X-Sqreen-Integrity': 'x'})
    assert resp.status_code == 500

    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    resp = client.post('/populate_sqnotif',
                       headers={'X-Sqreen-Integrity': 'x'},
                       content_type='application/json',
                       data=json.dumps({'a': 'b'}))
    assert resp.status_code == 200
