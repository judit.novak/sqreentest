import configparser

from populate_sqnotif.targets.email import EmailTarget


def test_populate():
    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'email'
    }
    config['target_email'] = {
        'to_address': 'me@example.com',
        'from_address': 'you@example.com',
        'smtp_server': 'localhost',
        'smtp_port': 1025
    }
    EmailTarget(config).populate({'hey': 'hoo'})
