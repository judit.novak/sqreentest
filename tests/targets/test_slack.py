import configparser

from populate_sqnotif.targets.slack import SlackTarget


def test_populate_slack(requests_mock):
    requests_mock.post('http://example.com')

    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'webhook_key': 'x',
        'targets': 'slack'
    }
    config['target_slack'] = {
        'webhook_url': 'http://example.com'
    }
    assert SlackTarget(config).populate({'a': 'b'})
