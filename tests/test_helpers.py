import configparser
import pytest

from populate_sqnotif.exceptions import PopulateException
from populate_sqnotif.helpers import check_config
from populate_sqnotif.helpers import pretty_data


def test_check_config():

    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'x': '1',
        'z': '',
    }

    assert check_config(config, ['x']) is None

    with pytest.raises(PopulateException) as err:
        check_config(config, ['x', 'y'])
    assert str(err.value) == ("Missing configuration parameter 'y'")

    with pytest.raises(PopulateException) as err:
        check_config(config, ['x', 'z'])
    assert str(err.value) == ("Missing configuration value for 'z'")


def test_pretty_data():

    data = {'y': [{'d': 1, 'c': 2}, {'b': 3}], 'x': 4}
    assert pretty_data(data) == '''\
{
    "x": 4,
    "y": [
        {
            "c": 2,
            "d": 1
        },
        {
            "b": 3
        }
    ]
}'''
