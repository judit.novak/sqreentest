POPULATE_HOSTIP=`ip route get 1 | sed 's/uid.*//' | awk '{print $$NF;exit}'`
POPULATE_PORT=5001

HELLO_HOSTIP=127.0.0.1
HELLO_PORT=5000


runserver:
	env FLASK_APP=populate_sqnotif/populate_sqnotif.py flask run -h ${POPULATE_HOSTIP} -p ${POPULATE_PORT}


runhello:
	env FLASK_APP=extras/testapp/hello.py flask run -h ${HELLO_HOSTIP} -p ${HELLO_PORT}


DEBUGSERVER_PID=/tmp/debugserver.pid
test:
	@echo "Starting DebugggingServer"
	@python -m smtpd -n -c DebuggingServer localhost:1025 > /dev/null 2>&1 &
	@echo "Running pytest" && pytest --cov tests
	@ps -ea -o pid,cmd | awk '/[D]ebuggingServer/ {print $$1}' > ${DEBUGSERVER_PID}
	@kill `cat < ${DEBUGSERVER_PID}` && \
		echo "Stopping DebugggingServer"
	@rm ${DEBUGSERVER_PID} || true
