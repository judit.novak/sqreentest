# Sqreentest 

A small Flask application consuming and re-dispatching notifications (alerts) from [Sqreen]
to different target channels via webhooks. 

(Within this `README` we suppose that you already had a basic introduction to [Sqreen].)

## Usage

Once you have a "Screen-ed" app to attach to, the simplest way to run the application is:

```
make runserver
```

This command will start up the web server on the host's public IP, port `5001`. These settings can be
overridden by the `POPULATE_HOSTIP` and `POPULATE_PORT` variables, as

```
make runserver POPULATE_HOSTIP=127.0.0.1 POPULATE_PORT=5001
```

A basic console logging facility allows to get information about the application activity and errors.


A small testjob is also added to the project, for test purposes. You can run it with the
```
make runhello
```
command (`localhost:5000`, to be overridden with `HELLO_HOSTIP`, `HELLO_PORT`). Once the 

1. `hello` application is registered with [Sqreen], 
2. and the `populate_sqnotif` application is running and registered as a [Sqreen] Webhook

[Sqreen] notifications should be published to configured channels.

## Description

The actual application is called `publish_sqnotif`: a Flask
application setting up a single endpoint (`/publish_sqnotif`),
waiting for `POST` requests from [Sqreen].

### Population Targets

Incoming [Sqreen] notifications will be distributed via configured channels to varios targets.
The current implementation includes *email* and *[Slack]* notifications. Corresponding
settings need to be fully configured in order to allow for the application to start. 

For configuration details please check the config file shipped with the project. All
parameters set there are mandatory.

 * *Slack*: Notifications are published to [Slack] using a webhook linked to a custom [Slack] application.

 * *Email*: Emails are constructed "traditionally" and sent via the SMTP server configured. 
    * (In this simple version of the application, authentication is not yet possible.)

### Extras

The package also contains a tiny "*Hello World*" application. In case you needed an app
to monitor with [Sqreen] :-)

Given the simplicity, it's moslty useful to have 'something' allowing to trigger notifications manually.




## Developers' Notes

The appliction's code is lightweight and modular, allowing easy extension for further
targest (SMS, etc.). All new *target* modules have to inherit from the common [`Target` 
anchestor inteface](./publish_sqnotif/targets/target.py).

### Testing

Testsuite is a mild mix of unit- and functional tests using `pytest`.
We suggest you to use the 
```
make test
``` 
command, as provides test environment setup as needed. 
(For example to test the `email` target we are using Python `smtpd.DebuggingServer`
running in the background.)


[sqreen]: http://sqreen.com
[slack]: http://slack.com
